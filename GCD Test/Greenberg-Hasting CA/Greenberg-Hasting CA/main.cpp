//Michael O'Neil
//B00239553

#include "Create_Bitmap.h"

int main(int argc, char *argv[])
{
    Create_Bitmap *newBitmapCreator = new Create_Bitmap();
	newBitmapCreator->run();
	delete newBitmapCreator;
    return 0;    
}
