//Michael O'Neil
//B00239553

#ifndef CREATE_BITMAP_H
#define CREATE_BITMAP_H

#include "intArray2BMP.hpp"

class Create_Bitmap
{
private:		
	static const int width = 640;
	static const int height = 480;		
	int ** map;			
public:
	Create_Bitmap();		
	void init(void);	
	void readInFrame(std::string fileName, int **myArray, int w, int h);	
	void run(void);		
};

#endif