#include "Create_Bitmap.h"
#include <cstdlib>
#include <stdlib.h>
#include <math.h>
#include <vector>

Create_Bitmap::Create_Bitmap()
{	

}

void Create_Bitmap::init(void)
{		
	map = new int*[width];
	for(int i = 0; i < width; ++i)
	{
		map[i] = new int[height];
	}
		
	//Change for file loading in.
	std::string currentFrame = "map.txt";
	//std::string currentFrame = "map.txt";
	readInFrame(currentFrame, map, width, height);	
		
	//Bitmap creator
	bool result = intarray2bmp::intarray2bmp("gradientMap.bmp", map, 640, 480, 0, 255);	
	
}

void Create_Bitmap::readInFrame(std::string fileName, int **myArray, int w, int h)
{	
	std::ifstream in_file;
	int input;
	in_file.open(fileName.c_str());
	if(!in_file.is_open())	
		std::cout << "File could not open!!" << std::endl;	
	else
		std::cout << "File opened!" << std::endl;

	for(int i = 0; i < h; ++i)
		for(int j =0; j < w; ++j)
		{
			in_file >> input;
			myArray[j][i] = input;
		}
	in_file.close();		
}

void Create_Bitmap::run(void)	
{
	init();
	//Delete 2d array created for map.
	for(int i = 0; i < width; ++i)
	{
		delete map[i];	
		map[i] = NULL;
	}
	delete [] map;	
	system("pause");	
}