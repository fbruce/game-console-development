#Subdirectories
DIRS := spu

#Target
PROGRAM_ppu := Coursework_prog

#Don't optimise code
CC_OPT_LEVEL = -O0

#Local Defines
INSTALL_DIR = ~/lab3
INSTALL_FILES = $(PROGRAM_ppu)

#Imports
IMPORTS := spu/lib_lab3_spu.a -lspe2 -lX11 -lpthread

#buildutils/make.footer
include $(CELL_TOP)/buildutils/make.footer

