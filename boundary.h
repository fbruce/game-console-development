//Alpha
//GCD Coursework

#ifndef BOUNDARY_H
#define BOUNDARY_H

class boundary
{
private:	
	int x1;
	int x2;
	int y1;
	int y2;
	int density;
public:
	boundary();
	boundary(int mX1, int mX2, int mY1, int mY2, int mDensity);	
	int GetX1() {return x1;}
	int GetX2() {return x2;}
	int GetY1() {return y1;}
	int GetY2() {return y2;}
	int GetDensity() {return density;}
	void SetX1(int mX1) {x1 = mX1;}
	void SetX2(int mX2) {x2 = mX2;}
	void SetY1(int mY1) {y1 = mY1;}
	void SetY2(int mY2) {y2 = mY2;}
	void SetDensity(int mDensity) {density = mDensity;}
	bool boundaryOverlaps(boundary * tmpBoundary);
};

#endif