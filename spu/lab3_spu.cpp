#include <stdio.h>
#include <spu_mfcio.h>
#include <spu_intrinsics.h>
#include "simdmath/powf4.h"
#include "simdmath/sqrtf4_fast.h"

struct DataPointers{
	unsigned long long ppuToSpuPtr;
	unsigned long long spuToPpuPtr;
};

typedef union VECTOR4F
{
	struct
	{
		int x;
		int y;
		int z;
		int w;
	} e;
	int v[4];
	vector signed int vec;
} vector4f;

const int height = 480;		// array element address' arranged by column first
const int width = 4;
	
int inData[width+2] [height+2]__attribute__((aligned(128))); //aligned MFC/DMA
int outData[width][height]__attribute__((aligned(128))); //aligned MFC/DMA
DataPointers dataPointers __attribute__((aligned(16)));

vector4f sobel(int x, int y)
{
	vector4f topLft, lft, btmLft, topRght, rght, btmRght, top, btm;
	for (int i = 0; i < 4; i++){
		topLft.v[i] = inData[x - 1][y - 1 + i];
		lft.v[i] = 2 * inData[x - 1][y + i];
		btmLft.v[i] = inData[x - 1][y + 1 + i];

		topRght.v[i] = inData[x + 1][y - 1 + i];
		rght.v[i] = 2 * inData[x + 1][y + i];
		btmRght.v[i] = inData[x + 1][y + 1 + i];

		top.v[i] = 2 * inData[x][y - 1 + i];
		btm.v[i] = 2 * inData[x][y + 1 + i];
	}

	vector4f xGradient, yGradient, sum;
	//Calculate x Gradient
	xGradient.vec = spu_sub(spu_add(spu_add(lft.vec, topLft.vec), btmLft.vec),
		spu_add(spu_add(rght.vec, topRght.vec), btmRght.vec));
	//Calculate y Gradient
	yGradient.vec = spu_sub(spu_add(spu_add(top.vec, topLft.vec), topRght.vec),
		spu_add(spu_add(btm.vec, btmLft.vec), btmRght.vec));

	//Calculate sum
	vector float power = (vector float){ 2.0, 2.0, 2.0, 2.0 };
	vector float temp = spu_add(_powf4(spu_convtf(xGradient.vec, 0), power), _powf4(spu_convtf(yGradient.vec, 0), power));
	temp = _sqrtf4_fast(temp);
	sum.vec = spu_convts(temp, 0);

	for (int i = 0; i < 4; i++){
		if (sum.v[i] < 0)
			sum.v[i] = 0;
		if (sum.v[i] > 255)
			sum.v[i] = 255;
	}
	return sum;
}

void constructGradient(){
	vector4f temp;
	for (int i = 1; i < width + 1; i++)
		for (int j = 1; j < height + 1; j += 4){
			temp = sobel(i, j);
			outData[i-1][j-1] = temp.e.x;
			outData[i-1][j] = temp.e.y;
			outData[i-1][j+1] = temp.e.z;
			outData[i-1][j+2] = temp.e.w;
		}		
}

int main (unsigned long long spe_id, 
			unsigned long long argp, 
			unsigned long long envp)		
{

	//Initiate copy from main RAM to local store
	mfc_get(&dataPointers, argp, sizeof(dataPointers), 0, 0, 0);
	mfc_write_tag_mask(1<<0);
	mfc_read_tag_status_any();

	unsigned int threadID = spu_read_in_mbox();
	
	unsigned long long inAddr = dataPointers.ppuToSpuPtr;
	unsigned long long outAddr = dataPointers.spuToPpuPtr;
	const int inChunk = (height+2) * width;	// chunk size for incoming data needs to account for the extra two rows of zeros
	const int outChunk = height * width;	//size of the data chunks 

	inAddr += inChunk * 40 * threadID * sizeof(int);
	outAddr += outChunk * 40 * threadID * sizeof(int);
	
	for (int i = 0; i < 40; i++){
		mfc_get(&inData, inAddr, sizeof(inData), 0, 0, 0);
		mfc_write_tag_mask(1<<0);
		mfc_read_tag_status_all();
			
		constructGradient();
			
		mfc_put(outData, outAddr, sizeof(outData), 0, 0, 0);
		mfc_write_tag_mask(1<<0);
		mfc_read_tag_status_all();
	
		inAddr += inChunk * sizeof(int);					// byte offset to move address to the start of the next chunk
		outAddr += outChunk * sizeof(int);
	}
	
	return 0;
}
