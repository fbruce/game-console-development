//Alpha
//GCD Coursework
//main.cpp

#include <iostream>
#include <fstream>
#include <string>
#include <time.h>
#include <vector>
#include "boundary.h"
#include <ppu_intrinsics.h>
#include <libspe2.h>
#include <pthread.h>

#define iThreads 4

const int width = 640;
const int height = 480;	
std::vector<boundary*> cellBoundaries;
const int TICKS_PER_SEC = 79800000;
clock_t startTime;
clock_t endTime;

extern spe_program_handle_t lab3_spu; //Program Handle

struct DataPointers{
	unsigned long long ppuToSpuPtr;
	unsigned long long spuToPpuPtr;
};
DataPointers dataPointers __attribute__((aligned(16)));

/*Function to read in file from file name provided. Will put 
data in to an array passed through.
@param - std::string - name of filename to read in.
@param - int[][] - 2d array to insert data into.
*/
void readInFrame(std::string fileName, int myArray[width+2][height+2])
{	
	std::ifstream in_file;
	int input;
	in_file.open(fileName.c_str());
	if(!in_file.is_open())	
		std::cout << "File could not open!!" << std::endl;	
	else
		std::cout << "File opened!" << std::endl;

	for(int i = 1; i < height+1; i++)
		for (int j = 1; j < width+1; j++){
			in_file >> input;
			myArray[j][i] = input;
		}	
	in_file.close();	
}

/*Function to write the gradient map to file as another 
program is to be used to create bitmap file.
@param - int[][] - 2d array, data from array is written to file.
*/
void writeMap(int myArray[width][height])
{
	std::ofstream myFile ("map.txt");
	if(myFile.is_open())
	{		
		for(int i = 0; i < height; ++i)
		{
			for(int j = 0; j < width; ++j)
			{
				myFile << myArray[j][i];
				if(myArray[j][i] < 10)
					myFile << "    ";
				else if(myArray[j][i] < 100)
					myFile << "   ";
				else
					myFile << "  ";
			}
			myFile << "\n";
		}	
		myFile.close();
	}
	else
		std::cout << "Unable to open file" << std::endl;	
}

/*Function will take in gradientMap, will search for high density areas and create
a boundary around them. In cases where boundaries may overlap will find the highest 
density area and create/change boundary to stop overlapping cases in output.
@param - int - width of array, required for searching data.
@param - int - height of array, required for searching data.
@param - int** - 2d array to search data from.
*/
void findCell(int x, int y, int image[width][height])
{
	//Variables required	
	int density = 0;		
	int boxWidth = 60;
	int boxHeight = 60;	
	int threshold = boxWidth * boxHeight * 75;

	//Set box starting at pixel [0][0], move box along so surrounds every pixel
	//on image to check for highest density.
	for(int i = 0; i < x - boxWidth; i++)
	{		
		for(int j = 0; j < y - boxHeight; j++)
		{			
			//Add up density for current box			
			for(int k = i; k < (i + boxWidth); k++)
				for(int l = j; l < (j + boxHeight); l++)
					density += image[k][l];
			if(density > threshold) //Only update if density higher than threshold
			{
				if(cellBoundaries.empty())
				{
					cellBoundaries.push_back(new boundary(i, i+boxWidth, j, j + boxHeight, density));
					density = 0;
				}
				else //Possible boundary so check against current boundaries to ensure doesnt overlap
				{
					bool overlaps = false;
					int pos = 0;
					boundary * tmpBox = new boundary(i, i+boxWidth, j, j + boxHeight, density);
					while(overlaps != true && pos < cellBoundaries.size())
					{
						//check if overlaps occurs.
						if(cellBoundaries[pos]->boundaryOverlaps(tmpBox) == true)
							overlaps = true;
						else
							pos++;
						
					}					
					if(overlaps == true)
					{
						if(density > cellBoundaries[pos]->GetDensity()) //Change existing boundary to higher density boundary
						{
							cellBoundaries[pos]->SetDensity(tmpBox->GetDensity());
							cellBoundaries[pos]->SetX1(tmpBox->GetX1());
							cellBoundaries[pos]->SetX2(tmpBox->GetX2());
							cellBoundaries[pos]->SetY1(tmpBox->GetY1());
							cellBoundaries[pos]->SetY2(tmpBox->GetY2());											
						}
						density = 0;
					}
					else //create new boundary
					{						
						cellBoundaries.push_back(new boundary(tmpBox->GetX1(), tmpBox->GetX2(), tmpBox->GetY1(), tmpBox->GetY2(), tmpBox->GetDensity()));
						density = 0;
					}	
					delete tmpBox;
					tmpBox = NULL;
				}					
			}
			else
				density = 0;
		}
	}	
}

void* ppu_pthread_function(void* arg)
{
	spe_context_ptr_t context;
	unsigned int entry = SPE_DEFAULT_ENTRY;
	context = *((spe_context_ptr_t*)arg);
	if (spe_context_run(context, &entry, 0, &dataPointers, NULL, NULL) < 0)
	{
		perror("Failed executing SPE program");
		exit(1);
	}
	pthread_exit(NULL);
}

int main()
{
	// frame array has two extra rows and coloumns to pad the edges with zeros
	// it saves the need for checking when on the edges for the sobel masks
	int frame[width + 2][height + 2]__attribute__((aligned(128))) = { 0 };
	int gradientMap[width][height]__attribute__((aligned(128))) = { 0 };

	unsigned int threadID;

	dataPointers.ppuToSpuPtr = 0 + (unsigned)frame;
	dataPointers.spuToPpuPtr = 0 + (unsigned)gradientMap;
	
	//Change for file loading in.
	std::string currentFrame = "Frame1.txt";
	readInFrame(currentFrame, frame);	

	std::vector<spe_context_ptr_t> vecSPEContexts;		//Context
	std::vector<pthread_t> vecThreads;

	vecSPEContexts.resize(iThreads);
	vecThreads.resize(iThreads);
	
	startTime = __mftb();
	//Create the SPE threads
	for (int i = 0; i < iThreads; i++){
		threadID = i;

		if ((vecSPEContexts[i] = spe_context_create(0, NULL)) == NULL)
		{
			perror("Failed creating SPE context");
			exit(1);
		}

		//Load the program handle
		if (spe_program_load(vecSPEContexts[i], &lab3_spu))
		{
			perror("Failed loading SPE program");
			exit(1);
		}

		//create thread
		if (pthread_create(&vecThreads[i], NULL, &ppu_pthread_function, &vecSPEContexts[i]))
		{
			perror("failed creating POSIX thread");
			exit(1);
		}

		spe_in_mbox_write(vecSPEContexts[i], &threadID, 1, SPE_MBOX_ANY_NONBLOCKING);
	}

	for (int i = 0; i < iThreads; i++)
	{
		if (pthread_join(vecThreads[i], NULL))
		{
			perror("Failed pthread_join");
		}

		//destroy context
		if (spe_context_destroy(vecSPEContexts[i]) != 0)
		{
			perror("Failed destroying context");
			exit(1);
		}
	}

	endTime = __mftb();
	int tickTimeResult = endTime - startTime;
	float timeResult = endTime - startTime;
	printf(" Time taken in ticks : %i ", tickTimeResult);
	printf("\nTime taken in seconds : %f \n", timeResult / TICKS_PER_SEC);

	findCell(width, height, gradientMap);

	//Change boundary values to 500 so bitmap reader knows to change colour.
	for(int i = 0; i < cellBoundaries.size(); i++)
	{
		for(int j = cellBoundaries[i]->GetX1(); j <= cellBoundaries[i]->GetX2(); j++)
		{
			gradientMap[j][cellBoundaries[i]->GetY1()] = 500;
			gradientMap[j][cellBoundaries[i]->GetY2()] = 500;
		}
		for(int k = cellBoundaries[i]->GetY1(); k <= cellBoundaries[i]->GetY2(); k++)
		{
			gradientMap[cellBoundaries[i]->GetX1()][k] = 500;
			gradientMap[cellBoundaries[i]->GetX2()][k] = 500;
		}
	}

	writeMap(gradientMap);

	std::cout << cellBoundaries.size() << std::endl;
	while(!cellBoundaries.empty())
	{
		delete cellBoundaries[0];	
		cellBoundaries.erase(cellBoundaries.begin());			
	}
	return 0;
}