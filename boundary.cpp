//Alpha
//GCD Coursework

#include "boundary.h"

boundary::boundary()
{

}
	
//Sets square position and its current state.
boundary::boundary(int mX1, int mX2, int mY1, int mY2, int mDensity)
{
	x1 = mX1;
	x2 = mX2;
	y1 = mY1;
	y2 = mY2;
	density = mDensity;
}

bool boundary::boundaryOverlaps(boundary * tmpBoundary)
{
	if(tmpBoundary->GetX1() >= x1 && tmpBoundary->GetX1() <= x2)
		if((tmpBoundary->GetY1() >= y1 && tmpBoundary->GetY1() <= y2) || (tmpBoundary->GetY2() >= y1 && tmpBoundary->GetY2() <= y2))
			return true;
	if(tmpBoundary->GetX2() >= x1 && tmpBoundary->GetX2() <= x2)
		if((tmpBoundary->GetY1() >= y1 && tmpBoundary->GetY1() <= y2) || (tmpBoundary->GetY2() >= y1 && tmpBoundary->GetY2() <= y2))
			return true;
	return false;
}